# loroadmin

SPA Administrator Stores, and Products for loro.com.co

#### Git Clone

`$ git clone https://danieljx@bitbucket.org/danieljx/loroadmin.git`

#### NPM Install

`$ cd loroadmin/`

`$ npm install`

#### Start App

`$ npm start`<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

#### User Login

|  User |  Pass |
|:-----:|:-----:|
| admin | 12345 |

#### Componentes Principales
```
    db/                 ----> Datos JSON
      products.json
      stores.json
      subsidiarys.json
      users.json
    src/
      Components/
        Footer.js
        Header.js
        Loading.js
        Main.js         ----> Componentes Main
        Products/       ----> Componentes Productos
          Edit/Edit.js
          List/List.js
          List/Pagination.js
          List/Tools.js
        Sidebar.js
        Sign/
        Stores/         ----> Componentes Tiendas
          Edit/Edit.js
          List/List.js
          List/Pagination.js
          List/Tools.js
        Subsidiarys/    ----> Componentes Sucursales
          Edit/Edit.js
          List/List.js
          List/Pagination.js
          List/Tools.js
      Redux/            ----> Implementación Redux

```

## ¿Qué características considera que tiene un buen código?
  #### Principalmente Organizado (Lógica, Archivos)