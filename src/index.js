import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';
import Redux from './Redux';

ReactDOM.render(
  <Provider store={Redux}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('LoroAdmin')
);
