import React, { Component } from 'react';
import { Route, Redirect } from "react-router-dom";
import { Container, Row } from 'reactstrap';
import Header from './Header';
import Sidebar from './Sidebar';
import Stores from './Stores/Stores';
import Subsidiarys from './Subsidiarys/Subsidiarys';
import Products from './Products/Products';
export default class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: this.props.user
        }
    }
    render() {
        return (
            <div>
                <Header user={this.props.user} logOut={this.props.logOut}/>
                <Container fluid>
                    <Row>
                        <Sidebar/>
                        <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                            <Route path="/store/:idStore" component={Stores} />
                            <Route path="/stores" component={Stores} />
                            <Route path="/subsidiary/:idSubsidiary" component={Subsidiarys} />
                            <Route path="/subsidiarys" component={Subsidiarys} />
                            <Route path="/product/:idProduct" component={Products} />
                            <Route path="/products" component={Products} />
                            <Redirect from="/" to="/stores"  />
                        </main>
                    </Row>
                </Container>
            </div>
        )
    }
}