import React, { Component } from 'react';
import { Table, ButtonGroup, Button, UncontrolledTooltip, CardImg } from 'reactstrap';
import { Link, withRouter } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCog, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stores: this.props.stores
        }
    }
    render() {
        return (
            <Table className="list-stores table-striped">
                <thead className="thead-dark">
                    <tr>
                        <th>
                            <FontAwesomeIcon icon={faCog} />
                        </th>
                        <th>Logo</th>
                        <th>Name</th>
                        <th>Subsidiarys</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.stores.map((store, index) => {
                        return (
                            <tr key={index}>
                                <th className="action" scope="row">
                                    <ButtonGroup size="sm">
                                        <Link id={"editButton" + index} to={"/store/" + store.id} className="btn btn-info">
                                            <FontAwesomeIcon icon={faEdit} />
                                        </Link>
                                        <UncontrolledTooltip placement="bottom" target={"editButton" + index}>
                                            Edit
                                        </UncontrolledTooltip>
                                        <Button id={"deleteButton" + index} color="danger">
                                            <FontAwesomeIcon icon={faTrash} />
                                        </Button>
                                        <UncontrolledTooltip placement="bottom" target={"deleteButton" + index}>
                                            Delete
                                        </UncontrolledTooltip>
                                    </ButtonGroup>
                                </th>
                                <td className="logo">
                                    <CardImg className="img-fluid" src={"/img/stores/" + store.logo} alt={store.name} />
                                </td>
                                <td className="name">{store.name}</td>
                                <td className="subsidiarys">{store.subsidiarys.length}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        )
    }
}
export default withRouter(List);