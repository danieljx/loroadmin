import React, { Component } from 'react';
import { Card, CardHeader, CardBody, CardFooter } from 'reactstrap';
import Tools from './List/Tools';
import List from './List/List';
import Pagination from './List/Pagination';
import Edit from './Edit/Edit';
import '../../Styles/Stores.css';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faList, faEdit } from '@fortawesome/free-solid-svg-icons';
import { withRouter } from "react-router-dom";
class Stores extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stores: this.props.stores,
            idStore: this.props.match.params.idStore,
            store: false
        }
    }
    componentDidMount() {
        if(this.state.idStore) {
            if(this.state.idStore !== 'add') {
                let stores = this.state.stores.filter(el => el.id === parseFloat(this.state.idStore));
                for(let st in stores) {
                    this.setState({store:stores[st]});
                }
            } else {
                this.setState({store:{
                    "id": null,
                    "name": "",
                    "logo": "",
                    "subsidiarys": []
                }});
            }
        }
    }
    render() {
        return (
            this.state.store ? (
                <Card id="stores">
                    <CardHeader>
                        <h4 className="float-left"><FontAwesomeIcon icon={faEdit} /> {this.state.store.name}</h4>
                    </CardHeader>
                    <CardBody>
                        <Edit store={this.state.store}/>
                    </CardBody>
                </Card>
            ) : (
                <Card id="stores">
                    <CardHeader>
                        <h4 className="float-left"><FontAwesomeIcon icon={faList} />Stores</h4>
                        <Tools/>
                    </CardHeader>
                    <CardBody>
                        <List stores={this.state.stores}/>
                    </CardBody>
                    <CardFooter>
                        <Pagination/>
                    </CardFooter>
                </Card>
            )
        )
    }
}

const mapStateToProps = (state) => {
    return {
        stores: state.stores
    };
};
const wrapper = connect(mapStateToProps);
const component = wrapper(Stores);

export default withRouter(component);