import React, { Component } from 'react';
import {  
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    UncontrolledTooltip,
    CardImg
} from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import '../Styles/Header.css';
export default class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: this.props.user,
            isAuthenticated: this.props.isAuthenticated,
            isOpen: false
        };
        this.toggleNav = this.toggleNav.bind(this);
        this.getPicture = this.getPicture.bind(this);
    }
    toggleNav() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    getPicture() {
        return (
            <svg className="bd-placeholder-img img-thumbnail" width="25" height="25" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="A generic square placeholder image with a white border around it, making it resemble a photograph taken with an old instant camera: 200x200"><title>A generic square placeholder image with a white border around it, making it resemble a photograph taken with an old instant camera</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="50%" y="50%" fill="#dee2e6" dy=".3em">{this.state.user.name.charAt(0) + this.state.user.lastname.charAt(0)}</text></svg>
        )
    }
    render() {
        return (
            <Navbar className="navbar flex-md-nowrap p-0 shadow" expand="md" fixed="top">
                <NavbarBrand href="/"><CardImg className="img-fluid" src="/logo.svg" alt="Loro Admin App"/></NavbarBrand>
                <NavbarToggler onClick={this.toggleNav} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="px-3 ml-auto" navbar>
                        <NavItem>
                            <NavLink className="nav-link">
                                {
                                    this.props.user.picture ? (
                                        <img className="d-inline-block align-top" width="25" height="25" src={"/img/users/" + this.props.user.id + "/" + this.props.user.picture} alt={this.state.user.name + ' ' + this.state.user.lastname}/>
                                    ):(
                                        this.getPicture()
                                    )
                                }
                                {this.state.user.name + " " + this.state.user.lastname}
                            </NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink id="signOut" className="px-3" href="#" role="button" onClick={this.props.logOut}>
                                <FontAwesomeIcon icon={faSignOutAlt} />
                            </NavLink>
                            <UncontrolledTooltip placement="bottom" target="signOut">
                                Signout
                            </UncontrolledTooltip>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        )
    }
}