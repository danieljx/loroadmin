import React, { Component } from 'react';
import { Table, ButtonGroup, Button, UncontrolledTooltip } from 'reactstrap';
import { Link, withRouter } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCog, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            subsidiarys: this.props.subsidiarys
        }
    }
    render() {
        return (
            <Table className="list-subsidiarys table-striped">
                <thead className="thead-dark">
                    <tr>
                        <th>
                            <FontAwesomeIcon icon={faCog} />
                        </th>
                        <th>Name</th>
                        <th>Products</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.subsidiarys.map((subsidiary, index) => {
                        return (
                            <tr key={index}>
                                <th className="action" scope="row">
                                    <ButtonGroup size="sm">
                                        <Link id={"editButton" + index} to={"/subsidiary/" + subsidiary.id} className="btn btn-info">
                                            <FontAwesomeIcon icon={faEdit} />
                                        </Link>
                                        <UncontrolledTooltip placement="bottom" target={"editButton" + index}>
                                            Edit
                                        </UncontrolledTooltip>
                                        <Button id={"deleteButton" + index} color="danger">
                                            <FontAwesomeIcon icon={faTrash} />
                                        </Button>
                                        <UncontrolledTooltip placement="bottom" target={"deleteButton" + index}>
                                            Delete
                                        </UncontrolledTooltip>
                                    </ButtonGroup>
                                </th>
                                <td className="name">{subsidiary.name}</td>
                                <td className="subsidiarys">{subsidiary.products.length}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        )
    }
}
export default withRouter(List);