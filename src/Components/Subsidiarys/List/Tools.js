import React, { Component } from 'react';
import { ButtonGroup, InputGroup, InputGroupAddon, UncontrolledTooltip } from 'reactstrap';
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
export default class Tools extends Component {
    render() {
        return (
            <InputGroup className="float-right">
                <InputGroupAddon addonType="append">
                    <ButtonGroup className="float-right" size="sm">
                        <Link id="newStoresButton" to={"/subsidiary/add"} className="btn btn-primary">
                            <FontAwesomeIcon icon={faPlus} />
                        </Link>
                        <UncontrolledTooltip placement="bottom" target="newStoresButton">
                            New Subsidiary
                        </UncontrolledTooltip>
                    </ButtonGroup>
                </InputGroupAddon>
            </InputGroup>
        )
    }
}