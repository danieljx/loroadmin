import React, { Component } from 'react';
import { Card, CardHeader, CardBody, CardFooter } from 'reactstrap';
import Tools from './List/Tools';
import List from './List/List';
import Pagination from './List/Pagination';
import Edit from './Edit/Edit';
import '../../Styles/Subsidiarys.css';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faList, faEdit } from '@fortawesome/free-solid-svg-icons';
import { withRouter } from "react-router-dom";
class Stores extends Component {
    constructor(props) {
        super(props);
        this.state = {
            subsidiarys: this.props.subsidiarys,
            idSubsidiary: this.props.match.params.idSubsidiary,
            subsidiary: false
        }
    }
    componentDidMount() {
        if(this.state.idSubsidiary) {
            if(this.state.idSubsidiary !== 'add') {
                let subsidiarys = this.state.subsidiarys.filter(el => el.id === parseFloat(this.state.idSubsidiary));
                for(let st in subsidiarys) {
                    this.setState({subsidiary:subsidiarys[st]});
                }
            } else {
                this.setState({subsidiary:{
                    "id": null,
                    "name": "",
                    "location": {
                        "latitude": "",
                        "longitud": ""
                    },
                    "products": []
                }});
            }
        }
    }
    render() {
        return (
            this.state.subsidiary ? (
                <Card id="subsidiarys">
                    <CardHeader>
                        <h4 className="float-left"><FontAwesomeIcon icon={faEdit} /> {this.state.subsidiary.name}</h4>
                    </CardHeader>
                    <CardBody>
                        <Edit subsidiary={this.state.subsidiary}/>
                    </CardBody>
                </Card>
            ) : (
                <Card id="subsidiarys">
                    <CardHeader>
                        <h4 className="float-left"><FontAwesomeIcon icon={faList} />Subsidiarys</h4>
                        <Tools/>
                    </CardHeader>
                    <CardBody>
                        <List subsidiarys={this.state.subsidiarys}/>
                    </CardBody>
                    <CardFooter>
                        <Pagination/>
                    </CardFooter>
                </Card>
            )
        )
    }
}

const mapStateToProps = (state) => {
    return {
        subsidiarys: state.subsidiarys
    };
};
const wrapper = connect(mapStateToProps);
const component = wrapper(Stores);

export default withRouter(component);