import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStoreAlt, faShoppingCart, faStore } from '@fortawesome/free-solid-svg-icons';
import '../Styles/Sidebar.css';
export default class Sidebar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: this.props.user,
            navigate: "stores"
        };
    }
    setNavigate(to) {
        this.setState({ navigate: to });
    }
    render() {
        return (
            <nav className="col-md-2 d-none d-md-block bg-light sidebar">
                <div className="sidebar-sticky">
                    <ul className="nav flex-column">
                        <li className="nav-item">
                            <Link to="/stores" className={"nav-link" + (this.state.navigate==="stores"?" active":"")} onClick={()=>{this.setNavigate("stores")}}>
                                <FontAwesomeIcon icon={faStoreAlt} className="feather feather-home"/>
                                Stores
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/subsidiarys" className={"nav-link" + (this.state.navigate==="subsidiarys"?" active":"")} onClick={()=>{this.setNavigate("subsidiarys")}}>
                                <FontAwesomeIcon icon={faStore} className="feather feather-home"/>
                                Subsidiarys
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/products" className={"nav-link" + (this.state.navigate==="products"?" active":"")} onClick={()=>{this.setNavigate("products")}}>
                                <FontAwesomeIcon icon={faShoppingCart} className="feather feather-home"/>
                                Products
                            </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}