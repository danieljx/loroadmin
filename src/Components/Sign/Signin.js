import React, { Component } from 'react';
import '../../Styles/Sign.css';
import { Row, Col, Container, Button, Form, FormGroup, Input, ButtonGroup, InputGroup, InputGroupAddon, InputGroupText, Alert, CardImg } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignInAlt, faLock, faUser } from '@fortawesome/free-solid-svg-icons';
export default class Signin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: false,
            pass: false
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChangeUser = this.handleChangeUser.bind(this);
        this.handleChangePass = this.handleChangePass.bind(this);
        this.onLogin = this.onLogin.bind(this);
    }
    onLogin() {
        this.props.logIn(this.state.user, this.state.pass);
    }
    handleSubmit(e) {
        e.preventDefault();
        this.onLogin();
        return false;
    }
    handleChangeUser(e) {
        this.setState({ user: e.target.value });
        return false;
    }
    handleChangePass(e) {
        this.setState({ pass: e.target.value });
        return false;
    }
    render() {
        return (
            <Container className="login-container">
                <Row>
                    <Col md="6" className="login-form">
                        <h3><CardImg className="img-fluid" src="/logo.svg" alt="Loro Admin App"/></h3>
                        { this.props.response &&
                            <Alert color={this.props.response.error?"danger":"success"}>{this.props.response.message}</Alert>
                        }
                        <Form onSubmit={this.handleSubmit}>
                            <FormGroup>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <FontAwesomeIcon icon={faUser} />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="text" id="user" name="user" placeholder="User" onChange={this.handleChangeUser} />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <InputGroup>
                                    <InputGroupAddon addonType="prepend">
                                        <InputGroupText>
                                            <FontAwesomeIcon icon={faLock} />
                                        </InputGroupText>
                                    </InputGroupAddon>
                                    <Input type="password" id="pass" name="pass" placeholder="Password" onChange={this.handleChangePass} />
                                </InputGroup>
                            </FormGroup>
                            <FormGroup>
                                <ButtonGroup>
                                    <Button className="btnSubmit">
                                        <span>Login</span>
                                        <FontAwesomeIcon icon={faSignInAlt} className="iconToLogin" />
                                    </Button>
                                </ButtonGroup>
                            </FormGroup>
                        </Form>
                    </Col>
                </Row>
            </Container>
        )
    }
}