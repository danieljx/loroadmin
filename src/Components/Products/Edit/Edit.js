import React, { Component } from 'react';
import { Form, FormGroup, Input, ButtonGroup, Button, InputGroup, InputGroupAddon, InputGroupText, UncontrolledTooltip, Alert } from 'reactstrap';
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronCircleLeft, faSave } from '@fortawesome/free-solid-svg-icons';
export default class Edit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: this.props.product,
            response: false
        }
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeLogo = this.handleChangeLogo.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChangeName(e) {
        console.log(e.targetCurrent.value);
    }
    handleChangeLogo(e) {
        console.log(e.targetCurrent.value);
    }
    handleSubmit(e) {
        e.preventDefault();
        console.log("handleSubmit");
        return false;
    }
    render() {
        return (
        <div>
            { this.state.response &&
                <Alert color={this.state.response.error?"danger":"success"}>{this.state.response.message}</Alert>
            }
            <Form onSubmit={this.handleSubmit}>
                <FormGroup>
                    <InputGroup>
                        <InputGroupAddon addonType="prepend">
                            <InputGroupText>
                                Name:
                            </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" name="name" placeholder="Name" value={this.state.product.name} onChange={this.handleChangeName} />
                    </InputGroup>
                </FormGroup>
                <FormGroup>
                    <ButtonGroup>
                        <Link id="backToListButton" to="/products" className="btn btn-warning">
                            <FontAwesomeIcon icon={faChevronCircleLeft} />
                        </Link>
                        <UncontrolledTooltip placement="bottom" target="backToListButton">
                            Back to List
                        </UncontrolledTooltip>
                        <Button id="saveButton" color="success">
                            <FontAwesomeIcon icon={faSave} />
                        </Button>
                        <UncontrolledTooltip placement="bottom" target="saveButton">
                            Save Store
                        </UncontrolledTooltip>
                    </ButtonGroup>
                </FormGroup>
            </Form>
            </div>
        )
    }
}