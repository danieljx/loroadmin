import React, { Component } from 'react';
import { Card, CardHeader, CardBody, CardFooter } from 'reactstrap';
import Tools from './List/Tools';
import List from './List/List';
import Pagination from './List/Pagination';
import Edit from './Edit/Edit';
import '../../Styles/Products.css';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faList, faEdit } from '@fortawesome/free-solid-svg-icons';
import { withRouter } from "react-router-dom";
class Products extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: this.props.products,
            subsidiarys: this.props.subsidiarys,
            stores: this.props.stores,
            idProduct: this.props.match.params.idProduct,
            product: false
        }
    }
    componentDidMount() {
        if(this.state.idProduct) {
            if(this.state.idProduct !== 'add') {
                let products = this.state.products.filter(el => el.id === parseFloat(this.state.idProduct));
                for(let st in products) {
                    this.setState({product:products[st]});
                }
            } else {
                this.setState({product:{
                    "id": null,
                    "name": "",
                    "img": [],
                    "qty": 0,
                    "price": 0,
                    "categories": []
                }});
            }
        }
    }
    render() {
        return (
            this.state.product ? (
                <Card id="products">
                    <CardHeader>
                        <h4 className="float-left"><FontAwesomeIcon icon={faEdit} /> {this.state.product.name}</h4>
                    </CardHeader>
                    <CardBody>
                        <Edit product={this.state.product}/>
                    </CardBody>
                </Card>
            ) : (
                <Card id="products">
                    <CardHeader>
                        <h4 className="float-left"><FontAwesomeIcon icon={faList} />Products</h4>
                        <Tools/>
                    </CardHeader>
                    <CardBody>
                        <List products={this.state.products} subsidiarys={this.state.subsidiarys} stores={this.state.stores}/>
                    </CardBody>
                    <CardFooter>
                        <Pagination/>
                    </CardFooter>
                </Card>
            )
        )
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.products,
        subsidiarys: state.subsidiarys,
        stores: state.stores
    };
};
const wrapper = connect(mapStateToProps);
const component = wrapper(Products);

export default withRouter(component);