import React, { Component } from 'react';
import { Table, ButtonGroup, Button, UncontrolledTooltip, CardImg, Badge } from 'reactstrap';
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCog, faEdit, faTrash } from '@fortawesome/free-solid-svg-icons';
export default class List extends Component {
    constructor(props) {
        super(props);
        this.props.products.map((product, x) => {
            this.props.subsidiarys.map((subsidiary, y) => {
                if(subsidiary.products.indexOf(product.id)>=0) {
                    this.props.products[x].subsidiaryName = subsidiary.name;
                    this.props.stores.map((store, z) => {
                        if(store.subsidiarys.indexOf(subsidiary.id)>=0) {
                            this.props.products[x].storeName = store.name;
                        }
                        return store;
                    });
                }
                return subsidiary;
            });
            return product;
        });
        this.state = {
            products: this.props.products,
            subsidiarys: this.props.subsidiarys,
            stores: this.props.stores
        }
    }
    render() {
        return (
            <Table className="list-products table-striped">
                <thead className="thead-dark">
                    <tr>
                        <th>
                            <FontAwesomeIcon icon={faCog} />
                        </th>
                        <th>Store</th>
                        <th>Subsidiary</th>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Categories</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.products.map((product, index) => {
                        return (
                            <tr key={index}>
                                <th className="action" scope="row">
                                    <ButtonGroup size="sm">
                                        <Link id={"editButton" + index} to={"/product/" + product.id} className="btn btn-info">
                                            <FontAwesomeIcon icon={faEdit} />
                                        </Link>
                                        <UncontrolledTooltip placement="bottom" target={"editButton" + index}>
                                            Edit
                                        </UncontrolledTooltip>
                                        <Button id={"deleteButton" + index} color="danger">
                                            <FontAwesomeIcon icon={faTrash} />
                                        </Button>
                                        <UncontrolledTooltip placement="bottom" target={"deleteButton" + index}>
                                            Delete
                                        </UncontrolledTooltip>
                                    </ButtonGroup>
                                </th>
                                <td className="store">{product.storeName}</td>
                                <td className="subsidiary">{product.subsidiaryName}</td>
                                <td className="name">{product.name}</td>
                                <td className="logo">
                                    <CardImg className="img-fluid" src={"/img/products/" + product.id + "/" + product.img[0]} alt={product.name} />
                                </td>
                                <td className="price">{product.price}</td>
                                <td className="qty">{product.qty}</td>
                                <td className="categories">
                                    {product.categories.map((category, x) => {
                                        return (
                                            <Badge key={x} color="dark">{category}</Badge>
                                        );
                                    })}
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        )
    }
}