import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStepBackward, faStepForward, faCaretRight, faCaretLeft } from '@fortawesome/free-solid-svg-icons';
export default class Tools extends Component {
    render() {
        return (
            <nav aria-label="Page navigation example">
                <ul className="pagination">
                    <li className="page-item">
                        <a className="page-link" href="#">
                            <FontAwesomeIcon icon={faStepBackward} />
                        </a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">
                            <FontAwesomeIcon icon={faCaretLeft} />
                        </a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">1</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">2</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">3</a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">
                            <FontAwesomeIcon icon={faCaretRight} />
                        </a>
                    </li>
                    <li className="page-item">
                        <a className="page-link" href="#">
                            <FontAwesomeIcon icon={faStepForward} />
                        </a>
                    </li>
                </ul>
            </nav>
        )
    }
}