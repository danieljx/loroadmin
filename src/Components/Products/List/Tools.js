import React, { Component } from 'react';
import { ButtonGroup, InputGroup, InputGroupAddon, UncontrolledTooltip } from 'reactstrap';
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
export default class Tools extends Component {
    render() {
        return (
            <InputGroup className="float-right">
                <InputGroupAddon addonType="append">
                    <ButtonGroup className="float-right" size="sm">
                        <Link id="newStoresButton" to={"/product/add"} className="btn btn-primary">
                            <FontAwesomeIcon icon={faPlus} />
                        </Link>
                        <UncontrolledTooltip placement="bottom" target="newStoresButton">
                            New Product
                        </UncontrolledTooltip>
                    </ButtonGroup>
                </InputGroupAddon>
            </InputGroup>
        )
    }
}