import React from 'react';
import { CardImg } from 'reactstrap';
import '../Styles/Loading.css';
export default () => {
    return (
        <div id="circle">
            <CardImg className="img-fluid" src="/logo.png" alt="Loading"/>
        </div>
    )
}