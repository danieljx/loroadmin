import { createStore, combineReducers } from 'redux';
import stores from './Stores/reducer';
import subsidiarys from './Subsidiarys/reducer';
import products from './Products/reducer';

const reducer = combineReducers({
    stores,
    subsidiarys,
    products
});

const redux = createStore(reducer);

export default redux;