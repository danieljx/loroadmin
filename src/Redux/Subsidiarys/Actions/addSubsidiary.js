export const type = 'addSubsidiary';

const addSubsidiary = subsidiary => {
    return {
        type,
        payload: subsidiary,
    };
};

export default addSubsidiary;