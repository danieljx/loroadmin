import subsidiarysState from '../../../db/subsidiarys.json';

function reducer(state = subsidiarysState, { type, payload }) {
    switch (type) {
        case 'addSubsidiary': {
            return payload.subsidiary
        }
        default:
            return state;
    }
}

export default reducer;