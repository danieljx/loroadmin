export const type = 'addStore';

const addStore = store => {
    return {
        type,
        payload: store,
    };
};

export default addStore;