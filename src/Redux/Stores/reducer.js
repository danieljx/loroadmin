import storesState from '../../../db/stores.json';

function reducer(state = storesState, { type, payload }) {
    switch (type) {
        case 'addStore': {
            return payload.store
        }
        default:
            return state;
    }
}

export default reducer;