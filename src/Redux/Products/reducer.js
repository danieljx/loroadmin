import productsState from '../../../db/products.json';

function reducer(state = productsState, { type, payload }) {
    switch (type) {
        case 'addProduct': {
            return payload.product
        }
        default:
            return state;
    }
}

export default reducer;