export const type = 'addProduct';

const addProduct = product => {
    return {
        type,
        payload: product,
    };
};

export default addProduct;