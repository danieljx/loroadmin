import React, { Component } from 'react';
import { Switch, Route, Redirect } from "react-router-dom";
import { Container } from 'reactstrap';
import Signin from './Components/Sign/Signin';
import Main from './Components/Main';
import Footer from './Components/Footer';
import Loading from './Components/Loading';
import './Styles/App.css';
import Users from '../db/users.json';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      isAuthenticated: false,
      user: false,
      response: false
    }
    this.logIn = this.logIn.bind(this);
    this.logOut = this.logOut.bind(this);
  }
  componentDidMount() {
    this.setState({ isLoading: false });
  }
  logIn(user, pass) {
    if(user !== "" && pass !== "") {
      Users.map((userx,index)=>{
        if(userx.user === user) {
          if(userx.pass === pass) {
            this.setState({ 
              user: userx,
              isAuthenticated: true 
            });
            return {
              error: false,
              message: "Success"
            };
          } else {
            this.setState({ 
              response: {
                error: true,
                message: "Error Login.!"
              },
              isAuthenticated: false 
            });
          }
        } else {
          this.setState({ 
            response: {
              error: true,
              message: "Error Login.!"
            },
            isAuthenticated: false 
          });
        }
        return userx;
      });
    } else {
      this.setState({ 
        response: {
          error: true,
          message: "Error Login.!"
        },
        isAuthenticated: false 
      });
    }
  }
  logOut() {
    this.setState({ 
      user: false,
      isAuthenticated: false 
    });
  }
  render() {
    return (
      <Container fluid >
        { !this.state.isLoading ?
          <Switch>
            <Route path="/login">
              {
                !this.state.isAuthenticated ? (
                  <Signin logIn={this.logIn} response={this.state.response} />
                ) : (
                  <Redirect
                    to={{
                      pathname: "/"
                    }}
                  />
                )
              }
            </Route>
            <Route path="/">
              {
                this.state.isAuthenticated ? (
                  <Main
                    user={this.state.user}
                    logOut={this.logOut}
                    isAuthenticated={this.state.isAuthenticated}
                  />
                ) : (
                  <Redirect
                    to={{
                      pathname: "/login"
                    }}
                  />
                )
              }
            </Route>
          </Switch> : <Loading/>
        }
        <Footer />
      </Container>
    );
  }
}

export default App;
